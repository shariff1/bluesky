package com.blue.sky.you.config;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Tokeninfo;
import com.google.api.services.oauth2.model.Userinfoplus;

import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class OAuth2Security {
  public static final String APPLICATION_NAME = "BlueSkyConnect";
  public static final java.io.File DATA_STORE_DIR =
      new java.io.File(System.getProperty("user.home"), ".store/oauth2_security");
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
  /** OAuth 2.0 scopes. */
  private static final List<String> SCOPES =
      Arrays.asList(
          "https://www.googleapis.com/auth/userinfo.profile",
          "https://www.googleapis.com/auth/userinfo.email");
  public static Oauth2 oauth2;
  private static FileDataStoreFactory fileDataStoreFactory;
  private static HttpTransport httpTransport;
  private static GoogleClientSecrets googleClientSecrets;
  private static Logger logger = Logger.getLogger("OAuth");

  public static void main(String args[]) {
    try {
      httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      fileDataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
      Credential credential = authorize();
      tokenInfo(credential.getAccessToken());
      userInfo();
    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void tokenInfo(String accessToken) throws IOException {
    System.out.println("Validating a Token");
    Tokeninfo tokeninfo = oauth2.tokeninfo().setAccessToken(accessToken).execute();
    System.out.println(tokeninfo.toPrettyString());
    if (!tokeninfo.getAudience().equals(googleClientSecrets.getDetails().getClientId())) {
      System.err.println("ERROR: audience does not match our client ID!");
    }
  }

  private static void userInfo() throws IOException {
    System.out.println("Obtaining User Profile Information");
    Userinfoplus userinfo = oauth2.userinfo().get().execute();
    System.out.println(userinfo.toPrettyString());
  }

  private static Credential authorize() throws IOException {
    googleClientSecrets =
        GoogleClientSecrets.load(
            JSON_FACTORY,
            new InputStreamReader(
                OAuth2Security.class.getResourceAsStream(
                    "/client_secret_145131018923-ii9995p1tri5kjpa432q58acq205grq4.apps.googleusercontent.com.json")));
    if (googleClientSecrets.getDetails().getClientId().startsWith("Enter")
        || googleClientSecrets.getDetails().getClientSecret().startsWith("Enter")) {
      logger.info("Generate id and secret from google console or check for null id and secret");
      System.exit(1);
    }
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, JSON_FACTORY, googleClientSecrets, SCOPES)
            .setDataStoreFactory(fileDataStoreFactory)
            .build();

    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
  }
}
